Assignment to aplikacja webowa, która została stworzona w celach edukacyjnych. Aplikacja jest przeznaczona zarówno dla kursantów jak i nauczycieli.
Aplikacja Assignment wykorzystuje Javę, Spring, Hibernate, SQL, Thymeleaf, JavaScript oraz Websocket.

Assignment udostępnia takie funkcję jak :
- rejestrację użytkówników z podziałem na nauczyciela oraz kursanta
- logowanie użytkownika
- dodawanie oraz modyfikację zadania
- dodawanie oraz modyikfację quizu
- czat grupowy
- podgląd obecności użytkowników

Dodatkowo nauczyciel może:
- upubliczniać zadania i quizy dla kursantów
- na bieżąco śledzić postęp zadania u każdego kursanta
- zmieniać grupę

Sprawdź: www.assignment.com.pl
______________________________________________________________________________________________________________________________________________________________________________
Assignment is a web application, which was created for educational purposes.The application is designed for both students and teachers.
The Assignment application uses Java, Spring, Hibernate, SQL, Thymeleaf, JavaScript and Websocket.

Assignment provides many features, such as:
- user registration divided into teacher and student
- user login
- adding and modifying tasks
- adding and modifying quizzes
- group chat
- user presence preview

In addition, the teacher can:
- make tasks and quizzes public for students
- keep track of the task progress of individual student 
- change the group

Check: www.assignment.com.pl