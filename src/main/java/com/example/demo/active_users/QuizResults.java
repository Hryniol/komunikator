package com.example.demo.active_users;

import com.example.demo.model.quiz.QuizAnswersForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuizResults {
    private List<QuizAnswersForm> results;
}
