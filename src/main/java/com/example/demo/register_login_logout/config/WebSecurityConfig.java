package com.example.demo.register_login_logout.config;

import com.example.demo.active_users.ActiveUsersService;
import com.example.demo.register_login_logout.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@ComponentScan("com.example.demo")
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


private final ActiveUsersService activeUsersService;
private final UserService userService;

//    @Autowired
//    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//       // String password = passwordEncoder().encode("password");
//
//        auth
//                .userDetailsService(this.userDetailsService).passwordEncoder(passwordEncoder());

//                temp
//                .inMemoryAuthentication()
//                .withUser("user")
//                .password(password)
//                .roles("USER")
//                .and()
//                .withUser("admin")
//                .password(password)
//                .roles("ADMIN","USER");
//               temp
//    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**","/webjars/**","/img/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers( "/css/**","/js/**","/form").permitAll()
                .antMatchers("/","/menu","/login**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .usernameParameter("username")
                .passwordParameter("password")
//                .failureUrl("/login?fail=true")
//                .successForwardUrl("/home")
                .failureForwardUrl("/login-error")
                .successHandler(successHandler())
                .permitAll()

                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .addLogoutHandler(logoutHandler())
                .logoutSuccessUrl("/login?logout=true")
                .permitAll()

        ;
    }

    @Bean
    AuthenticationSuccessHandler successHandler(){return new CustomAuthenticationSuccessHandler(userService,activeUsersService);}

    @Bean
    LogoutHandler logoutHandler(){
        return new CustomLogoutHandler(activeUsersService);}

}
