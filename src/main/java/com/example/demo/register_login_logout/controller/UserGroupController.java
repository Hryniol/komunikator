package com.example.demo.register_login_logout.controller;

import com.example.demo.active_users.ActiveUsersService;
import com.example.demo.controllers.IndexControllers.Util.UtilClass;
import com.example.demo.register_login_logout.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class UserGroupController {
    private final ActiveUsersService activeUsersService;

    @GetMapping("/changeGroup/{id}")
    public String changeGroup(@PathVariable(name = "id")Long id){
        User loggedInUser = UtilClass.getLoggedInUser();
        Long prevGroupId = loggedInUser.getActiveGroupId();
        loggedInUser.setActiveGroupId(id);

        activeUsersService.updateActiveUser(loggedInUser,prevGroupId);
        return "redirect:/index";
    }
}
