package com.example.demo.register_login_logout.controller;

import com.example.demo.active_users.ActiveUsersService;
import com.example.demo.register_login_logout.model.CourseRole;
import com.example.demo.register_login_logout.model.User;
import com.example.demo.register_login_logout.service.UserService;
import com.example.demo.services.UserTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequiredArgsConstructor
public class LoginLogoutController {
    private final UserService userService;
    private final ActiveUsersService activeUsersService;
    private final UserTestService userTestService;
    @RequestMapping(value = "/user-redirect")
    public String redirectHomePage(HttpServletRequest request, Authentication authentication){

        String userName = authentication.getName();
        User user = userService.findByUserName(userName);
        HttpSession session = request.getSession(false);
        session.setAttribute("appUser", user);

        activeUsersService.addActiveUser(user);

        String roleUrl = ((user.getCourseRole() == CourseRole.TEACHER) ? "home-t" : "home-s");
        return String.format("redirect:/%s",roleUrl);
    }

    @RequestMapping(value = "/logout")
    public String logOut(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        User user = (User)session.getAttribute("appUser");

        activeUsersService.removeActiveUser(user);

        return "login";
    }

    @RequestMapping(value = "/login-error")
    public String loginError(Model model){

        System.out.println("Login fail");
        model.addAttribute("loginError",true);
        return "login";
    }

}


