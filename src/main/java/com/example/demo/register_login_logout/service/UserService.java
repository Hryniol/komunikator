package com.example.demo.register_login_logout.service;


import com.example.demo.controllers.IndexControllers.Util.UtilClass;
import com.example.demo.model.entity_IndexController.Grupa;
import com.example.demo.model.entity_IndexController.UserTest;
import com.example.demo.register_login_logout.model.Role;
import com.example.demo.register_login_logout.model.User;
import com.example.demo.register_login_logout.model.UserForm;
import com.example.demo.register_login_logout.model.repository.RoleRepository;
import com.example.demo.register_login_logout.model.repository.UserRepository;
import com.example.demo.services.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final BCryptPasswordEncoder encoder;

    private final GroupService groupService;

       public void save(User user) {
           userRepository.save(user);
           user.setRoles(new HashSet<>(roleRepository.findAll()));
           user.setPassword(encoder.encode(user.getPassword()));
    }

    public User findByUserName(String username) {
        return userRepository.findByUsername(username);
    }

    public User findById(Long id){ return userRepository.findById(id).orElseThrow(()->new IllegalArgumentException("User with such id doesn't exist."));}


    public List<User> findAll() {
        return userRepository.findAll();
    }


    public void addRoles(String roleName){
        Role role = new Role();
        role.setName(roleName);
        roleRepository.save(role);
    }

    public void updateUser(Long id, UserForm user, List<Grupa> groups){
            User u = findById(id);
            u.setActiveGroupId(user.getActiveGroupId());
            u.setUsername(user.getUsername());
            u.setPassword(user.getPassword());
            u.setPasswordConfirm(user.getPasswordConfirm());
            u.setCourseRole(user.getCourseRole());
            u.setGroups(groups);
            userRepository.save(u);
            UtilClass.updateLoggedInUser(u);
        }

        public void updateUserActiveGroup(Long userId, Long groupId){
           User u = findById(userId);
           if(u.getGroups().contains(groupService.findById(groupId))){
           u.setActiveGroupId(groupId);
           userRepository.save(u);
           }
        }

}
