package com.example.demo.register_login_logout.model.repository;


import com.example.demo.register_login_logout.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
}
