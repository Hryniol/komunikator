package com.example.demo.register_login_logout.model;

import com.example.demo.model.entity_IndexController.Grupa;
import com.example.demo.model.lesson.Lesson;
import com.example.demo.model.lesson.LessonContent;
import com.example.demo.model.quiz.Quiz;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

@Entity
@Table
@Data
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String password;


    @Transient
    private String passwordConfirm;

    @ManyToMany
    private Set<Role> roles;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CourseRole courseRole;

//    @ManyToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE})
//    @JoinTable(name = "user_task",
//            joinColumns = @JoinColumn(name = "user_id"),
//            inverseJoinColumns = @JoinColumn(name = "task_id"))
//    List<Task> favouriteTasks;


    @ManyToMany (cascade = {CascadeType.PERSIST,CascadeType.MERGE},
            fetch = FetchType.EAGER)
    @JoinTable(name = "user_group",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "grupa_id"))
    List<Grupa> groups;


    private Long activeGroupId = 1L;


    @OneToMany(mappedBy = "author")
    private List<Quiz> quizzes;

    @ManyToMany(mappedBy = "students")
    private List<Lesson> studentLessons;

    @OneToMany(mappedBy = "teacher")
    private List<Lesson> teacherLessons;

    @ManyToMany(mappedBy = "teachers")
    private List<LessonContent> favouriteLessonContents;

    private Boolean isPreparingLessonContent;

    @Override
    public String toString() {
        return String.format("id:%s, username: %s, password: %s, passwordConfirm: %s",id, username, password, passwordConfirm);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this)return true;

        if(!(obj instanceof User))return false;

        User u = (User) obj;

        return (u.id == id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    public List<Lesson> getLessons(){
        if(courseRole == CourseRole.STUDENT) return studentLessons;
        return teacherLessons;
    }
}
