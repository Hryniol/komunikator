package com.example.demo.services;

import com.example.demo.controllers.IndexControllers.Util.UtilClass;
import com.example.demo.model.entity_IndexController.Category;
import com.example.demo.model.entity_IndexController.Task;
import com.example.demo.model.entity_IndexController.repo.CategoryRepo;
import com.example.demo.model.entity_IndexController.repo.TaskRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class TaskService {
    private final TaskRepo taskRepo;
    private final CategoryRepo categoryRepo;

    public Iterable<Task> getAll() {
        return taskRepo.findAll();
    }

    public Optional<Task> findById(@RequestParam Long index) {
        return taskRepo.findById(index);
    }

    public Optional<Task> findByIdKursu(@RequestParam Task task) {
        return taskRepo.findById(task.getZadania_dla_kursu().getId_kurs());
    }

    public Task addTask(@RequestBody Task Task) {
        return taskRepo.save(Task);
    }

    public void updateTask(Task task, Long id) {
        Optional<com.example.demo.model.entity_IndexController.Task> TaskbyId = findById(id);
        if (TaskbyId.isPresent()) {
            com.example.demo.model.entity_IndexController.Task task1 = TaskbyId.get();
            task1.setKategoria(task.getKategoria());
            task1.setTresc_zadania(task.getTresc_zadania());
            task1.setRozwiazanie_zadania(task.getRozwiazanie_zadania());
            task1.setWskazowka(task.getWskazowka());
            taskRepo.save(task1);
        }
    }

    public void deleteTask(Long index) {
        taskRepo.deleteById(index);
    }


    public List<Task> getListOfSelectedTask(String kategoria, Long idGrupy) {
        String category="";
        if (kategoria==null){
            category = categoryRepo.findAll().get(0).getKategoria();
        } else{
            category=kategoria;
        }
        List<Task> byKategoria = taskRepo.findAllByKategoriaAndIdgrupa(category, idGrupy);
        return byKategoria;
    }

}