package com.example.demo.services;

import com.example.demo.model.entity_IndexController.Category;
import com.example.demo.model.entity_IndexController.repo.CategoryRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
@RequiredArgsConstructor
@Service
public class CategoryService {
    private final CategoryRepo categoryRepo;

    public Iterable<Category> getAllcategory() {
        return categoryRepo.findAll();
    }

    public Optional<Category> findCategoryById(@RequestParam Long id) {
        return categoryRepo.findById(id);
    }

    public Category addcategory(@RequestBody Category cat) {
        return categoryRepo.save(cat);
    }

    public void deleteCategory(Long index) {
        categoryRepo.deleteById(index);
    }

    public Category getCategoryOrZero(Long id) {
        Optional<Category> categoryById = findCategoryById(id);
        Category ZeroCategory = new Category("");
        return categoryById.orElse(ZeroCategory);
    }
    public List<Category> ListOfCategory() {
        Iterator<Category> iterator = getAllcategory().iterator();
        List<Category> actualList = new ArrayList<Category>();
        while (iterator.hasNext()) {
            actualList.add(iterator.next());
        }
        return actualList;
    }
}


