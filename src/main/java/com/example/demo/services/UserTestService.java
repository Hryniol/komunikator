package com.example.demo.services;

import com.example.demo.controllers.IndexControllers.Util.UtilClass;
import com.example.demo.model.entity_IndexController.CheckBox;
import com.example.demo.model.entity_IndexController.Href;
import com.example.demo.model.entity_IndexController.UserTest;
import com.example.demo.model.entity_IndexController.repo.CheckBoxRepo;
import com.example.demo.model.entity_IndexController.repo.HrefRepo;
import com.example.demo.model.entity_IndexController.repo.UserTestRepo;
import com.example.demo.register_login_logout.model.CourseRole;
import com.example.demo.register_login_logout.model.User;
import com.example.demo.register_login_logout.model.repository.UserRepository;
import com.example.demo.register_login_logout.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserTestService {
    private final UserRepository userRepository;
    private final UserTestRepo userTestRepo;
    private final HrefRepo hrefRepo;
    private final CheckBoxRepo checkBoxRepo;

    public void saveInitialUser(String username) {
        User userTmp = userRepository.findByUsername(username);
        UserTest userTest = new UserTest();
        Href href = new Href();

        userTest.setId(userTmp.getId());
        userTest.setGroupId(userTmp.getActiveGroupId());
        userTest.setUsername(userTmp.getUsername());
        userTest.setRole(userTmp.getCourseRole());
        userTestRepo.save(userTest);

        href.setGroupID(userTmp.getActiveGroupId());
        hrefRepo.save(href);
    }

    public UserTest findByUserName(String username) {
        return userTestRepo.findByUsername(username).orElseThrow(() -> new IllegalArgumentException("brak takiego usera"));
    }

    public List<UserTest> findAll() {
        return userTestRepo.findAll();
    }

    public void saveStatus(String username, String status) {
        UserTest userTmp = findByUserName(username);
        userTmp.setTest_status(status);
        userTestRepo.save(userTmp);
    }

    public void saveStatusForStudents(Long id, String status) {
        List<UserTest> selected = userTestRepo.findAllByGroupId(id);
        selected.stream()
                .forEach(user -> {
                    user.setTest_status(status);
                    System.out.println(user.toString());
                    userTestRepo.save(user);
                });
    }

    public String loadStatus(String username) {
        UserTest userTmp = findByUserName(username);
        return userTmp.getTest_status();
    }

    public String loadHref(Long groupId) {
        Href HrefbyGroupID = hrefRepo.findByGroupID(groupId);
        return HrefbyGroupID.getHref();
    }

    public void saveHref(Long groupId, String href) {
        Href HrefbyGroupID = hrefRepo.findByGroupID(groupId);
        HrefbyGroupID.setHref(href);
        hrefRepo.save(HrefbyGroupID);
    }

    public void deleteUser(String username) {
        Optional<UserTest> OptionalUser = userTestRepo.findByUsername(username);
        if( OptionalUser.isPresent()){
            userTestRepo.delete(OptionalUser.get());
        }
    }
    public void saveCheckBox(CheckBox checkBox) {
        CheckBox checkBox1= new CheckBox();
        checkBox1.setGroupId(checkBox.getGroupId());
        checkBox1.setIsTipOn(checkBox.getIsTipOn());
        checkBox1.setIsAnswerOn(checkBox.getIsAnswerOn());
        checkBoxRepo.save(checkBox1);
    }


}

