package com.example.demo.model.lesson;

import com.example.demo.model.entity_IndexController.Task;
import com.example.demo.model.quiz.Quiz;
import com.example.demo.register_login_logout.model.User;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
@Data
@Entity
public class LessonContent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany (mappedBy = "lessonContent")
    private List<Lesson> lessons;

    @ManyToMany
    private List<Quiz> quizzes;

    @ManyToMany
    private List<Task> tasks;

    @ManyToMany
    private List<User> teachers;

    public LessonContent() {
        quizzes = new ArrayList<>();
        tasks = new ArrayList<>();
    }
}
