package com.example.demo.model.lesson;

import com.example.demo.model.entity_IndexController.Grupa;
import com.example.demo.register_login_logout.model.User;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate date;

    @ManyToOne
    private User teacher;

    @ManyToOne()
    private Grupa group;

    @ManyToMany
    private List<User> students;

    @ManyToOne
    private LessonContent lessonContent;

}
