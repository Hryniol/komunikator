package com.example.demo.model.entity_loginController.repo;

import com.example.demo.model.entity_loginController.Kurs;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface KursRepo extends CrudRepository<Kurs,Long> {
}
