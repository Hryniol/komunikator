package com.example.demo.model.entity_IndexController.repo;

import com.example.demo.model.entity_IndexController.Grupa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrupaRepo extends JpaRepository<Grupa,Long> {
    Grupa findByName(String groupname);
}
