package com.example.demo.model.entity_IndexController;

import com.example.demo.model.quiz.Quiz;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id_kategoria;
    @Column
    private String kategoria;

    @OneToMany(mappedBy = "category")
    private List<Quiz> quizzes;

    public Category(String kategoria) {
        this.kategoria = kategoria;
    }

    public Category() {
    }

    public long getId_kategoria() {
        return id_kategoria;
    }

    public void setId_kategoria(long id_kategoria) {
        this.id_kategoria = id_kategoria;
    }

    public String getKategoria() {
        return kategoria;
    }

    public void setKategoria(String kategoria) {
        this.kategoria = kategoria;
    }

    @Override
    public String toString() {
        return  "kategoria='" + kategoria + '\'' +
                '}';
    }
}
