package com.example.demo.model.entity_IndexController;

import com.example.demo.model.entity_loginController.Kurs;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long idPracyDomowej;

    @Column
    private Long idgrupa;
    @Column
    private String kategoria;
    @Column
    @NotNull
    private String tresc_zadania;
    @Column
    @NotNull
    private String rozwiazanie_zadania;
    @Column
    @NotNull
    private String wskazowka;
    @Column
    private String id_trenera;

    @ManyToOne
    @JoinColumn(name = "id_kurs")
    private Kurs zadania_dla_kursu;

         public Task(String kategoria, String tresc_zadania, String rozwiazanie_zadania, String wskazowka, String id_klient) {
        this.kategoria = kategoria;
        this.tresc_zadania = tresc_zadania;
        this.rozwiazanie_zadania = rozwiazanie_zadania;
        this.wskazowka = wskazowka;
        this.id_trenera = id_klient;
    }

    public Task(Kurs zadania_dla_kursu, Long idgrupa, Long id_pracy_domowej, String kategoria, String tresc_zadania, String rozwiazanie_zadania, String wskazowka, String id_klient) {
        this.zadania_dla_kursu = zadania_dla_kursu;
        this.idgrupa = idgrupa;
        this.idPracyDomowej = id_pracy_domowej;
        this.kategoria = kategoria;
        this.tresc_zadania = tresc_zadania;
        this.rozwiazanie_zadania = rozwiazanie_zadania;
        this.wskazowka = wskazowka;
        this.id_trenera = id_klient;
    }

    public Task(Long idgrupa, String kategoria, String tresc_zadania, String rozwiazanie_zadania, String wskazowka, String id_klient) {
        this.idgrupa = idgrupa;
        this.kategoria = kategoria;
        this.tresc_zadania = tresc_zadania;
        this.rozwiazanie_zadania = rozwiazanie_zadania;
        this.wskazowka = wskazowka;
        this.id_trenera = id_klient;
    }
    public Task( String kategoria, String tresc_zadania) {
        this.kategoria=kategoria;
        this.tresc_zadania = tresc_zadania;}



}

