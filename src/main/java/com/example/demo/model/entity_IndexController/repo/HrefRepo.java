package com.example.demo.model.entity_IndexController.repo;

import com.example.demo.model.entity_IndexController.Href;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HrefRepo extends JpaRepository<Href,Long> {
    Href findByGroupID(Long id);
}
