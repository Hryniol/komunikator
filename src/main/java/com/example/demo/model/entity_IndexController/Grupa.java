package com.example.demo.model.entity_IndexController;

import com.example.demo.model.lesson.Lesson;
import com.example.demo.register_login_logout.model.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class Grupa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    Long id_grupa;
    @Column
    String name;

    @ManyToMany (mappedBy = "groups")
    List<User> users = new ArrayList<>();

    @OneToMany (mappedBy = "group")
    private List<Lesson> lessons;

    public Grupa(String name) {
        this.id_grupa=id_grupa;
        this.name = name;
    }

    public Grupa() {
    }

    public Long getId_grupa() {
        return id_grupa;
    }

    public void setId_grupa(Long id_grupa) {
        this.id_grupa = id_grupa;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
