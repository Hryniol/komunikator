package com.example.demo.model.quiz;

import com.example.demo.register_login_logout.model.User;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class QuizForm {

    @NotNull
    private Long categoryId;

    private User author;

    @NotBlank
    private String question;

    @NotBlank
    private String answerA;
    private Boolean isAnswerACorrect;

    @NotBlank
    private String answerB;
    private Boolean isAnswerBCorrect;

    @NotBlank
    private String answerC;
    private Boolean isAnswerCCorrect;

    @NotBlank
    private String answerD;
    private Boolean isAnswerDCorrect;


}
