package com.example.demo.model.quiz;

import com.example.demo.model.entity_IndexController.Category;
import com.example.demo.register_login_logout.model.User;
import com.example.demo.register_login_logout.model.repository.UserRepository;
import com.example.demo.register_login_logout.service.UserService;
import com.example.demo.services.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class QuizService {
    private final QuizRepository quizRepository;
    private final UserService userService;
    private final CategoryService categoryService;
    private final AnswerService answerService;

    public void addQuiz(@NotNull QuizForm quizForm){
        Quiz quiz = new Quiz();

        quiz.setQuestion(quizForm.getQuestion());
        Category category = categoryService.findCategoryById(quizForm.getCategoryId())
                .orElseThrow(()-> new IllegalArgumentException("Category with such id doesn't exist."));
        quiz.setCategory(category);

//        User author = userService.findById(quizForm.getAuthorId())
//                .orElseThrow(()-> new IllegalArgumentException("User with such id doesn't exist."));

        quiz.setAuthor(quizForm.getAuthor());

        quizRepository.save(quiz);

        saveAnswers(quizForm,quiz);
    }


    public void update(QuizForm quizForm, Long id){
        Quiz quizToUpdate = findById(id);
        quizToUpdate.setQuestion(quizForm.getQuestion());
        Category category = categoryService.findCategoryById(quizForm.getCategoryId())
                .orElseThrow(()-> new IllegalArgumentException("Category with such id doesn't exist."));
        quizToUpdate.setCategory(category);

        List<Answer> answers = quizToUpdate.getAnswers();
        answerService.update(answers.get(0).getId(),quizForm.getAnswerA(),quizForm.getIsAnswerACorrect());
        answerService.update(answers.get(1).getId(),quizForm.getAnswerB(),quizForm.getIsAnswerBCorrect());
        answerService.update(answers.get(2).getId(),quizForm.getAnswerC(),quizForm.getIsAnswerCCorrect());
        answerService.update(answers.get(3).getId(),quizForm.getAnswerD(),quizForm.getIsAnswerDCorrect());

        quizRepository.save(quizToUpdate);

    }

    public List<Quiz> findAll(){return quizRepository.findAll();}
    public List<Quiz> findAllOrderByCategory(){return quizRepository.findByOrderByCategoryAsc();}

    public void deleteById(Long id){
        quizRepository.deleteById(id);
    }

    public List<Quiz> findAllByCategoryId(Long categoryId){return quizRepository.findAllByCategoryId(categoryId);}
    public List<Quiz> findAllByCategoryName(String categoryName){return quizRepository.findAllByCategoryName(categoryName);}

    public Quiz findById(Long id){return quizRepository.findById(id).orElseThrow(()->new IllegalArgumentException("No quiz with such id"));}

    public  void saveAnswers(QuizForm quizForm, Quiz quiz){
        Answer answerA = new Answer(quizForm.getAnswerA(), quizForm.getIsAnswerACorrect());
        answerA.setQuiz(quiz);
        answerService.save(answerA);

        Answer answerB = new Answer(quizForm.getAnswerB(),quizForm.getIsAnswerBCorrect());
        answerB.setQuiz(quiz);
        answerService.save(answerB);

        Answer answerC = new Answer(quizForm.getAnswerC(),quizForm.getIsAnswerCCorrect());
        answerC.setQuiz(quiz);
        answerService.save(answerC);

        Answer answerD = new Answer(quizForm.getAnswerD(),quizForm.getIsAnswerDCorrect());
        answerD.setQuiz(quiz);
        answerService.save(answerD);
    }
}
