package com.example.demo.model.quiz;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AnswerService {
    private final AnswerRepository answerRepository;

    public void save(Answer answer){answerRepository.save(answer);}

    public void update(Long id, String answer, Boolean isCorrect){
        Answer answerToUpdate = findById(id);
        answerToUpdate.setAnswer(answer);
        answerToUpdate.setIsCorrectAnswer(isCorrect);
        answerRepository.save(answerToUpdate);
    }

    public Answer findById(Long id){
        return answerRepository.findById(id)
                .orElseThrow(()->new IllegalArgumentException("Answer with such id doesn't exist"));
    }
}
