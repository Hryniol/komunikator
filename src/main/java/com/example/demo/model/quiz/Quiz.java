package com.example.demo.model.quiz;

import com.example.demo.model.entity_IndexController.Category;
import com.example.demo.register_login_logout.model.User;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Quiz {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Category category;

    private String question;

    @OneToMany(cascade = CascadeType.ALL ,mappedBy = "quiz")
    private List<Answer> answers;

    @ManyToOne
    private User author;

}
