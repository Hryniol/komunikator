package com.example.demo.controllers.IndexControllers.Util;

import com.example.demo.model.entity_IndexController.ServletPath;
import com.example.demo.register_login_logout.model.User;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class UtilClass {

    public static com.example.demo.register_login_logout.model.User getLoggedInUser(){
        HttpSession session = getCurrentHttpRequest().getSession(false);
        return (com.example.demo.register_login_logout.model.User)session.getAttribute("appUser");
    }
    public static HttpServletRequest getCurrentHttpRequest(){
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            HttpServletRequest request = ((ServletRequestAttributes)requestAttributes).getRequest();
            return request;
        }
        return null;
    }

    public static ServletPath getServletPath() {
        ServletPath servletPath = new ServletPath();
        servletPath.setPath(getCurrentHttpRequest().getContextPath());
        return servletPath;
    }

    public static void updateLoggedInUser(User updatedUser){
        HttpSession session = getCurrentHttpRequest().getSession(false);
        session.setAttribute("appUser",updatedUser);
    }

}
