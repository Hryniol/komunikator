package com.example.demo.controllers.IndexControllers;

import com.example.demo.active_users.ActiveUsersService;
import com.example.demo.model.entity_IndexController.*;
import com.example.demo.model.entity_IndexController.repo.CheckBoxRepo;
import com.example.demo.model.entity_IndexController.repo.HrefRepo;
import com.example.demo.services.MessengerService;
import com.example.demo.services.UserTestService;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
public class MessengerController {
    private final MessengerService messengerService;
    private final UserTestService userTestService;
    private final ActiveUsersService activeUsersService;
    private final HrefRepo hrefRepo;
    private final CheckBoxRepo checkBoxRepo;

    @MessageMapping("/messenger")
    @SendTo("/topic/messenger")
    public List<Messenger> getMessenger(MessengerPOJO messenger) throws InterruptedException {
        messengerService.addMessage(messenger,messenger.getGroupId());
        Thread.sleep(1000);
        return messengerService.findAllMessagesById(messenger.getGroupId());
    }


    @MessageMapping("/href/{groupId}")
    @SendTo("/topic/href/{groupId}")
    public Href addHrefForGroup(Href href, @DestinationVariable Long groupId) throws InterruptedException {
        userTestService.saveHref(groupId, href.getHref());
        Href HrefbyGroupID = hrefRepo.findByGroupID(groupId);
        activeUsersService.updateActiveUsers(groupId);
        return HrefbyGroupID;
    }

    @MessageMapping("/personalstatus/{groupId}")
    @SendTo("/topic/personalstatus/{groupId}")
    public UserTest changeStatus(UserTest userTest, @DestinationVariable Long groupId) throws InterruptedException {
        userTestService.saveStatus(userTest.getUsername(), userTest.getTest_status());
        activeUsersService.updateActiveUsers(groupId);
        return userTest;
    }

    @MessageMapping("/allstatus/{groupId}")
    @SendTo("/topic/allstatus/{groupId}")
    public UserTest changeAllStatus(UserTest userTest, @DestinationVariable Long groupId) throws InterruptedException {
        userTestService.saveStatusForStudents(userTest.getGroupId(), userTest.getTest_status());
        Thread.sleep(1000);
        activeUsersService.updateActiveUsers(groupId);
        return userTest;
    }

    @MessageMapping("/checkbox/{groupId}")
    @SendTo("/topic/checkbox/{groupId}")
    public CheckBox showOrHideTipAndAnswer(CheckBox checkBox, @DestinationVariable Long groupId) throws InterruptedException {
        userTestService.saveCheckBox(checkBox);
        return checkBoxRepo.findByGroupId(groupId);
    }

}
