package com.example.demo.controllers.IndexControllers;

import com.example.demo.active_users.ActiveUsersPackage;
import com.example.demo.active_users.ActiveUsersService;
import com.example.demo.controllers.IndexControllers.Util.UtilClass;
import com.example.demo.model.entity_IndexController.*;
import com.example.demo.model.entity_loginController.Login_Password;
import com.example.demo.model.entity_loginController.repo.Login_PasswordRepo;
import com.example.demo.model.lesson.LessonContent;
import com.example.demo.model.quiz.Quiz;
import com.example.demo.model.quiz.QuizAnswersForm;
import com.example.demo.register_login_logout.model.User;
import com.example.demo.services.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Data
@RequiredArgsConstructor
@ControllerAdvice
@Controller
public class IndexController {
    private Login_PasswordRepo login_passwordRepo;
    private final TaskService taskService;
    private final CategoryService categoryService;
    private final GroupService groupService;
    private final MessengerService messengerService;
    private final KursService kursService;
    private final ActiveUsersService activeUsersService;
    private final UserTestService userTestService;
    private String Login;

    @GetMapping(value = {"/index", "/home-t", "/home-s"})
    public String get(Model model, HttpServletRequest request) {
        return "index";
    }

    @ModelAttribute
    public void AddAttributes(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        User appUser = null;
        if (session != null) appUser = (User) session.getAttribute("appUser");
        if (appUser != null) {
            ServletPath servletPath = UtilClass.getServletPath();
            com.example.demo.register_login_logout.model.User user = UtilClass.getLoggedInUser();

            Long activeGroupId = user.getActiveGroupId();
            ActiveUsersPackage activeUsers = activeUsersService.getActiveUsersPackage(activeGroupId);
            Login = user.getUsername();

            //Logowanie
            model.addAttribute("allLogins", activeUsers);
            model.addAttribute("loggedInUser", user);
            model.addAttribute("contextpath", servletPath);
            model.addAttribute("nowyLoginOkno", new Login_Password());
            //Zadania
            model.addAttribute("href", userTestService.loadHref(activeGroupId));
            model.addAttribute("Tasks", taskService.getAll());
            model.addAttribute("noweZadanie", new Task());
            model.addAttribute("wybierz_zadanie", new Task());
            //Kategorie
            model.addAttribute("nowaKategoria", new Category());
            model.addAttribute("listOfCategory", categoryService.getAllcategory());
            //Messenger
            model.addAttribute("komunikator", new Messenger());
            model.addAttribute("wyswtekst", messengerService.findAllMessagesById(activeGroupId));
            //Grupy
            model.addAttribute("nowaGrupa", new Grupa());
            model.addAttribute("listOfGrupa", user.getGroups());
            //Quiz
            model.addAttribute("chosenCategoryIdQuizzes", new Quiz());
            model.addAttribute("newQuizPath","/showQuiz/");
            model.addAttribute("QuizAnswersForm",new QuizAnswersForm());

            model.addAttribute("newLessonContent",new LessonContent());

        }
    }
}

