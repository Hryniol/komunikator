package com.example.demo.controllers.IndexControllers;

import com.example.demo.controllers.IndexControllers.Util.UtilClass;
import com.example.demo.model.entity_IndexController.Task;
import com.example.demo.model.entity_IndexController.repo.TaskRepo;
import com.example.demo.services.CategoryService;
import com.example.demo.services.TaskService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;

@Data
@Controller
@RequiredArgsConstructor
public class SelectTaskController {
    private final TaskService taskService;
    private final TaskRepo taskRepo;
    private final CategoryService categoryService;

    @GetMapping("/showTasks")
    private String showTests(Model model,@RequestParam (required = false) String kategoria) throws AuthenticationException {
        Task task=new Task();
        task.setKategoria(kategoria);
        model.addAttribute("wybierz_zadanie",task);
        model.addAttribute("listOfPreSelectedTasks",taskService.getListOfSelectedTask(kategoria, UtilClass.getLoggedInUser().getActiveGroupId()));
        return "task/showTasksPage";}


    @GetMapping("/showtest")
    public String showSpecificTest(@RequestParam("id")Long id, Model model, HttpServletRequest request) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        model.addAttribute("singleTask",task);
        return "task/showSingleTaskPage";
    }

    @GetMapping("/verifytest")
    public String verifySpecificTest(@RequestParam("id")Long id, Model model, HttpServletRequest request) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        model.addAttribute("singleTask",task);
        return "task/verifySingleTaskPage";
    }



    @GetMapping("/edittest")
    public String editSpecificTest(@RequestParam("id")Long id, Model model, HttpServletRequest request) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        model.addAttribute("singleTask",task);
        return "task/editSingleTaskPage";
    }

    @PostMapping("/edittest")
    public String editwSpecificTest(@ModelAttribute("singleTask") Task task,@RequestParam("id")Long id,  HttpServletRequest request, RedirectAttributes redirectAttributes) throws AuthenticationException {
        String _ERROR = "";
        String _OK = null;
        String return_text = "redirect:/editTask"+id;
        String trescX = task.getTresc_zadania();
        String rozwiazanie = task.getRozwiazanie_zadania();
        String wskazowka = task.getWskazowka();

        if (trescX == null || trescX.isEmpty() || trescX.equals("") || trescX.trim().isEmpty() ) {
            _ERROR += "Treść zadania nie może być pusta" + System.lineSeparator();
            trescX = "";
        }
        if (rozwiazanie == null || rozwiazanie.isEmpty() || rozwiazanie.equals("") || rozwiazanie.trim().isEmpty() ) {
            _ERROR += "Rozwiązanie zadania nie może być puste" + System.lineSeparator();
            rozwiazanie = "";
        }
        if (wskazowka == null || wskazowka.isEmpty() || wskazowka.equals("") || wskazowka.trim().isEmpty() ) {
            _ERROR += "Wskazówka zadania nie może być puste" + System.lineSeparator();
            wskazowka = "";
        }
        else {
            _OK = "Edycja zadania przeprowadzona pomyślnie ";
            taskService.updateTask(task,id);
            _ERROR = null;
        }
        redirectAttributes.addFlashAttribute("trescX", trescX);
        redirectAttributes.addFlashAttribute("rozwiazanie", rozwiazanie);
        redirectAttributes.addFlashAttribute("wskazowka", wskazowka);
        redirectAttributes.addFlashAttribute("_ERROR", _ERROR);
        redirectAttributes.addFlashAttribute("_OK", _OK);
        if (_ERROR==null) {
            return_text = "redirect:/showTasks";
        }
     return return_text;
    }
    @GetMapping("/deletetest")
    public String deleteSpecificTest(@RequestParam("id")Long id,RedirectAttributes redirectAttributes) throws AuthenticationException {
        Task task= taskRepo.findById(id).orElseThrow(()->new IllegalArgumentException("Nie ma takiego zadania"));
        taskRepo.delete(task);
        String _OK="Zadanie #"+id+" usunięte.";
        redirectAttributes.addFlashAttribute("_OK", _OK);

        return "redirect:/showTasks";
    }


}


