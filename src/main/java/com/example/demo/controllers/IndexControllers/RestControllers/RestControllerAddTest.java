package com.example.demo.controllers.IndexControllers.RestControllers;

import com.example.demo.model.entity_IndexController.Task;
import com.example.demo.model.entity_IndexController.repo.CategoryRepo;
import com.example.demo.model.entity_IndexController.repo.TaskRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class RestControllerAddTest {
    private final TaskRepo taskRepo;
        private final CategoryRepo categoryRepo;

    @GetMapping("/kat/{kat}")
    public List<Task> getKat(@PathVariable("kat") String kategoria, RedirectAttributes redirectAttributes ) {
        List<Task> ListOfTasks = new ArrayList<>();
        if (kategoria != null && !kategoria.isEmpty()){
            redirectAttributes.addFlashAttribute("kategoriaRF",kategoria);
            ListOfTasks = taskRepo.findByKategoria((categoryRepo.findByKategoria(kategoria).getKategoria()));
        }
        return ListOfTasks;
    }
}
