package com.example.demo.controllers.LoginControllers;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SpringBean {

    @Autowired
    private ServletContext servletContext;
    @Value("#{servletContext.contextPath}")
    private String servletContextPath;
    @PostConstruct
    public void showIt() {
//        System.out.println("GET SERVLET CONTEXTPATH"+servletContext.getContextPath());
//        System.out.println("jeszze raz"+servletContextPath);
    }
}
