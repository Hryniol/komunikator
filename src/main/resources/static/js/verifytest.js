function TriggerTipOrAnswerOnYourOwn(){
    let checkTip=document.getElementById("wskazowkaCheckBoxV").checked;
    let checkAnswer=document.getElementById("rozwiazanieCheckBoxV").checked;

    let rozwiazanieStudent = document.getElementById("rozwiazanie_zadaniaV");
    let wskazowkaStudent = document.getElementById("wskazowkaV");
    let labelWskazowka=document.getElementById("labelWskazowkaV");
    let labelRozwiazanie=document.getElementById("labelRozwiazanieV");

    if (checkTip) { wskazowkaStudent.style.visibility = 'visible'; labelWskazowka.style.visibility='visible'; }
    else {  wskazowkaStudent.style.visibility = 'hidden'; labelWskazowka.style.visibility='hidden'; }
    if (checkAnswer) {rozwiazanieStudent.style.visibility = 'visible'; labelRozwiazanie.style.visibility='visible'; }
    else {
        rozwiazanieStudent.style.visibility = 'hidden'; labelRozwiazanie.style.visibility='hidden'; }
}
function goBack() {
    window.history.back();
}