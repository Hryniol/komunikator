const kategoria = document.querySelector('#kategoria');

const dodZad = document.querySelector('#dodZad');
const dodKat = document.querySelector('#dodKat');
const ButtonShowTasks = document.querySelector('#ButtonShowTasks');
const buttonAddQuiz = document.querySelector("#add_quiz");
const ButtonShowQuizTasks = document.querySelector("#ButtonShowQuizTasks");

$("#MessengerWindow").scrollTop($("#MessengerWindow")[0].scrollHeight);

function setCursor() {
    document.getElementById("input_tekst_for_window_okno").focus();
};


dodZad.addEventListener("click", () => {
    if (window.location.href.indexOf("/addtask") > -1 ) {
        window.location.href = '/index';
    } else {
        window.location.href = '/addtask';
    }});

dodKat.addEventListener("click", () => {
    if (window.location.href.indexOf("/addcategory") > -1 ) {
        window.location.href = '/index';
    } else {
        window.location.href = '/addcategory';
    }});

ButtonShowTasks.addEventListener("click", () => {
    if (window.location.href.indexOf("/showTasks") > -1 ) {
        window.location.href = '/index';
    }
    else {
        window.location.href = '/showTasks';
    }});

buttonAddQuiz.addEventListener("click",()=>{
    if (window.location.href.indexOf("/addQuiz") > -1 ) {
        window.location.href = '/index';
    } else {
        window.location.href = '/addQuiz';
    }});


$(function () {
    $(".close").click(function(){
        $("#myAlert").hide();
    });
});
$(function () {
    $(".close").click(function(){
        $("#myAlert2").hide();
    });
});
function goBack() {
    window.history.back();
}


