const innervalue = document.getElementById("kategoria");

function filltasklist() {
    let valueFromOption = innervalue.options[innervalue.selectedIndex].value;
    let role = document.getElementById("role").innerText;

    $('.kategoria option[value=valueFromOption]').attr('selected','selected');
    $(".tables").empty();
    $.get({
        url: "/kat/" + valueFromOption,
        success: function (data) {
            for (let i = 0; i < data.length; i++) {
                var table = document.createElement("TABLE");
                table.setAttribute("id", "tasksJS");
                table.setAttribute ('class', "table table-bordered table-striped mb-0");
                let thead = table.createTHead();
                thead.setAttribute ('class', "thead-dark");

                var rowN = thead.insertRow(0);
                let cell6 = rowN.insertCell(0);
                cell6.setAttribute("id", "cell6");
                let cell12 = rowN.insertCell(1);
                cell12.setAttribute("id", "cell12");
                let cell7 = rowN.insertCell(2);
                cell7.setAttribute("id", "cell7");
                let cell8 = rowN.insertCell(3);
                cell8.setAttribute("id", "cell8");

                cell6.innerHTML = "#";
                cell12.innerHTML = "Kategoria";
                cell7.innerHTML = "Modyfikacja";
                cell8.innerHTML = "Wybierz";

                var rowM = thead.insertRow(1);
                let cell9 = rowM.insertCell(0);
                let cell13 = rowM.insertCell(1);
                let cell10 = rowM.insertCell(2);
                let cell11 = rowM.insertCell(3);


                cell9.innerHTML = data[i].idPracyDomowej;
                cell13.innerHTML = data[i].kategoria;


                var btnE = document.createElement("a");

                btnE.setAttribute('data-toggle','tooltip');
                btnE.setAttribute('title','Edytuj zadanie');
                btnE.innerHTML = "<i class='fas fas fa-edit'></i>Edytuj";
                btnE.className = "d-inline btn btn-sm btn-rounded";
                btnE.setAttribute("id", "ButtonEditTest");
                cell10.insertAdjacentElement('afterbegin', btnE);
                btnE.setAttribute('href','/edittest?id='+data[i].idPracyDomowej);

                var btnD = document.createElement("a");
                btnD.setAttribute('data-toggle','tooltip');
                btnD.setAttribute('title','Usuń zadanie');
                btnD.innerHTML = "<i class='fas fas fa-times'></i>Usuń";
                btnD.className = "d-inline btn  btn-sm btn-warning btn-rounded";
                btnD.setAttribute("id", "ButtonDeleteTest");
                cell10.insertAdjacentElement('afterbegin', btnD);
                btnD.setAttribute('href','/deletetest?id='+data[i].idPracyDomowej);

                if (role=='TEACHER'){
                    var btnU = document.createElement("a");
                    btnU.setAttribute('data-toggle','tooltip');
                    btnU.setAttribute('title','Upublicznij zadanie uczniom');
                    btnU.innerHTML  = "<i class='fas fa-share-square'></i>Upublicznij";
                    btnU.className = "d-inline btn btn-sm btn-info btn-rounded";
                    btnU.setAttribute("id", "ButtonChosenTest");
                    cell11.insertAdjacentElement('afterbegin', btnU);
                    btnU.setAttribute('href','/showtest?id='+data[i].idPracyDomowej);
                    btnU.setAttribute('onclick','buttonchosentest(this.href)');}

                var btn = document.createElement("a");
                btn.setAttribute('data-toggle','tooltip');
                btn.setAttribute('title','Obejrzyj zadanie');
                btn.innerHTML = "<i class='fas   fa-file-alt'></i>Obejrzyj";
                btn.className = "d-inline btn btn-sm btn-rounded";
                btn.setAttribute("id", "ButtonVerifyTest");
                cell11.insertAdjacentElement('afterbegin', btn);
                btn.setAttribute('href','/verifytest?id='+data[i].idPracyDomowej);


                var table1 = document.createElement("TABLE");
                table1.setAttribute("id", "tableWithTests");
                table1.setAttribute ('class', "table table-bordered table-striped mb-0");

                var row = table1.insertRow(0);
                let cell0 = row.insertCell(0);
                cell0.setAttribute("id", "cell0");
                let cell1 = row.insertCell(1);
                cell1.setAttribute("id", "cell1");

                cell0.innerHTML = "treść zadania";
                cell1.innerHTML = data[i].tresc_zadania;

                $(".tables").append(table, table1);

            }

            console.log(table1)
        }

    })
    ;
}
